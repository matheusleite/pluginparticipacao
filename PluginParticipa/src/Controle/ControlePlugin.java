package Controle;

import Modelo.Administrador;
import Modelo.Usuario;

public class ControlePlugin {
	
	
	public static String EnviarPergunta(Usuario umUsuario, Administrador umAdmin){
		String pergunta = umUsuario.getPergunta().getQuestao();
		umAdmin.getPergunta().setQuestao(pergunta);
		return "Pergunta enviada ao administrador.";
	}
	
	public static String EnviarResposta(Usuario umUsuario, Administrador umAdmin){
		String resposta = umAdmin.getResposta().getResposta();
		umUsuario.getResposta().setResposta(resposta);
		return "Resposta enviada ao usuário. ";
	}
	
	public static String EncerrarAssunto(String encerrar){
		return "Assunto encerrado";
	}

}
