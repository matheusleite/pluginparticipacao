package Testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Controle.ControlePlugin;
import Modelo.Administrador;
import Modelo.Pergunta;
import Modelo.Resposta;
import Modelo.Usuario;

public class Testes {

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testeEnviarPergunta() {
		Usuario user = new Usuario();
		Administrador admin = new Administrador();
		Pergunta pergunta = new Pergunta();
		user.setPergunta(pergunta);
		admin.setPergunta(pergunta);
		String questao = "Teste";
		user.getPergunta().setQuestao(questao);
		assertEquals(ControlePlugin.EnviarPergunta(user,admin), "Pergunta enviada ao administrador.");
	}
	
	@Test
	public void testeEnviarResposta() {
		Usuario user = new Usuario();
		Administrador admin = new Administrador();
		Resposta resposta = new Resposta();
		user.setResposta(resposta);
		admin.setResposta(resposta);
		String umaResposta = "Teste";
		user.getResposta().setResposta(umaResposta);
		assertEquals(ControlePlugin.EnviarResposta(user,admin), "Resposta enviada ao usuário. ");
	}
	
	@Test
	public void testeEncerrar() {
		String teste = "Teste";
		assertEquals(ControlePlugin.EncerrarAssunto(teste), "Assunto encerrado");
	}

}
